<?php
/**
 * @file
 * Contains \Drupal\smart_ip_abstract_web_service\AbstractWebService.
 */

namespace Drupal\smart_ip_abstract_web_service;


class AbstractWebService {

  /**
   * Abstract IP Geolocation web service version 1 query URL.
   */
  const V1_URL = 'https://ipgeolocation.abstractapi.com/v1/';

}
